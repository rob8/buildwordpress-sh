# buildWordpress.sh

buildWordpress.sh is a bash script designed to quickly build new local Wordpress installations. The script requires a local MAMP stack configured like the one described in [http://twikenhampixelco.com/the-ultimate-wordpress-mamp-stack/](http://twikenhampixelco.com/the-ultimate-wordpress-mamp-stack/)

## To Use:

1. Create a MySQL database for the new Wordpress installation prior to running the script.
2. Open a terminal and navigate to the folder that contains the script. Run `sudo sh buildWordpress.sh` to run the script.