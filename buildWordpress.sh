#!/bin/bash

read -p 'WP Version: ' wpver
read -p 'Site Directory Name: ' sitename
read -p 'MySQL Database (created prior to running this script): ' dbname

svn export http://core.svn.wordpress.org/tags/$wpver ~/www/sites/$sitename/wwwroot

read -sp 'MySQL Root Password: ' sqlpass

cd ~/www/sites/$sitename/wwwroot
wp core config --dbname=$dbname --dbuser=root --dbpass=$sqlpass --dbhost=localhost --skip-check --allow-root

echo "Generated Wordpress config file"

read -p 'Site Name: ' wpsite
read -p 'Admin Email: ' email
read -p 'Wordpress Username: ' wpuser
read -sp 'Wordpress Password: ' wppass

wp core install --url=$sitename.dev --title="$wpsite" --admin_user=$wpuser --admin_password=$wppass --admin_email=$email --allow-root

chown -R _www:_www ~/www/sites/$sitename
chmod -R g+w ~/www/sites/$sitename

echo "Permissions set. Make sure that your username has been added to the _www group."

echo "Installed Wordpress to http://$sitename.dev"